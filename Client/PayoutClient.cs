﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Swish.SampleCode
{
    /// <summary>
    // / Performs the Requests to the Swish API
    /// </summary>
    public class PayoutClient
    {
        /// <summary>
        /// The Payout Base URL
        /// </summary>
        private HttpClient HttpClient;
        private readonly string PayoutBaseURL;
        private CertificateService CertificateService;

        public PayoutClient(string payoutBaseURL, CertificateService certService)
        {
            this.CertificateService = certService;
            this.PayoutBaseURL = payoutBaseURL;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ClientCertificates.Add(certService.GetClientCert());
            HttpClient = new HttpClient(clientHandler);
            HttpClient.DefaultRequestHeaders.Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Sends the POST request and waits for the result.
        /// </summary>
        /// <returns>The Response Message</returns>
        public async Task<HttpResponseMessage> CreatePayout(CreatePayoutData createPayoutData)
        {
            Console.WriteLine($"Sending POST request to: {PayoutBaseURL}" +
                $"\nRequest Content:\n{JsonConvert.SerializeObject(createPayoutData, Formatting.Indented)}");

            StringContent content = new StringContent(JsonConvert.SerializeObject(createPayoutData), Encoding.UTF8, "application/json");

            var postTask = HttpClient.PostAsync(PayoutBaseURL, content);
            return await postTask;
        }

        /// <summary>
        /// Polling for the payout status to be PAID.
        /// Sends GET requests every 0.4 secs and looks at json field "status" to be "PAID"
        /// Will retry maxTries number of times.
        /// </summary>
        /// <returns>The Response Message</returns>
        /// <param name="payoutLocation">Payout location / PayoutInstructionUUID</param>
        public async Task<bool> PollPayoutStatus(string payoutLocation)
        {
            Console.WriteLine($"Polling Payout Information until status == PAID. 20 tries. \nGET: {PayoutBaseURL + payoutLocation}");
            int maxTries = 20;
            bool succesfulPayout = false;
            HttpResponseMessage response = null;
            for (int numOfTries = 1; numOfTries < maxTries; numOfTries++)
            {
                var getTask = HttpClient.GetAsync(PayoutBaseURL + payoutLocation);
                response = await getTask;
                if(response.IsSuccessStatusCode)
                {
                    JObject jsonContent = JsonConvert.DeserializeObject<JObject>(await response.Content.ReadAsStringAsync());
                    String payoutStatus = jsonContent["status"].ToString(); 
                    if(payoutStatus == "PAID")
                    {
                        Console.WriteLine($"Try number: {numOfTries}/{maxTries}, status = {payoutStatus}. - Successful payout");
                        succesfulPayout = true;
                        break;
                    }
                    Console.WriteLine($"Try number: {numOfTries}/{maxTries}, status = {payoutStatus}. Retrying...");
                    Thread.Sleep(400);
                }
            }

            if(response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Payout Status (GET result):\n" +
                    $"{JsonConvert.SerializeObject(JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync()), Formatting.Indented)}");
            } 
            else 
            {
                Console.WriteLine($"\n\nUnsuccessful Request\nResponse:{response}\nContent: {await response.Content.ReadAsStringAsync()}");
            }
            return succesfulPayout;
        }
    }   
}
