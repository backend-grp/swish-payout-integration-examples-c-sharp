﻿namespace Swish.SampleCode
{

    /// <summary>
    /// The Data structure that is expected as Body for the POST Payout Request.
    /// </summary>
    public class CreatePayoutData
    {
        /// <summary>
        /// The Create Payout Data
        /// </summary>
        public CreatePayoutPayload payload;

        /// <summary>
        /// Base64 encoded signature of the hashed payload.
        /// </summary>
        public string signature;

        /// <summary>
        /// URL that Swish system will use to notify caller about the result of the payment request.
        /// Format: "https://..."     .
        /// Required: NO
        /// </summary>
        public string callbackUrl;

        public CreatePayoutData()
        {
            payload = new CreatePayoutPayload();
        }
    }
}
