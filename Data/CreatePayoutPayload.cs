﻿namespace Swish.SampleCode
{
    /// <summary>
    /// Contains the fields of the Payload, 
    /// The fields matches with the "payload"-JSONelement that API expects.
    /// </summary>
    public class CreatePayoutPayload
    {
        /// <summary>
        /// The Merchant Swish number.
        /// Required: YES
        /// </summary>
        public string payerAlias { get; set; }

        /// <summary>
        /// Merchant specific reference. This reference could be order id or similar.
        /// Required: NO
        /// </summary>
        public string payerPaymentReference { get; set; }

        /// <summary>
        /// Uniquely Identifying the Payout Instruction sent to Swish System.
        /// Format: 32 Chars Upper Case Hexadecimal.
        /// Required: YES
        /// </summary>
        public string payoutInstructionUUID { get; set; }

        /// <summary>
        /// The Swish number of the part to recieve the payout.
        /// Required: YES
        /// </summary>
        public string payeeAlias { get; set; }

        /// <summary>
        /// SSN of the payee. Will be validated against the enrolled SSN of the payee. 
        /// Format: YYYYMMDDXXXX
        /// Required: YES
        /// </summary>
        public string payeeSSN { get; set; }

        /// <summary>
        /// Amount to be paid. 
        /// ”.” or ”,” are accepted as decimal character. Digits after separator are optional.
        /// Required: YES
        /// </summary>
        public string amount { get; set; }

        /// <summary>
        /// Only ”SEK” is allowed.
        /// Required: YES
        /// </summary>
        public string currency { get; set; }

        /// <summary>
        /// Only "PAYOUT" is allowed.
        /// Required: YES
        /// </summary>
        public string payoutType { get; set; }

        /// <summary>
        /// The message from the merchant to the payee/reciever.
        /// Required: NO
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// Serial Number of the Signing Certificate
        /// Format: Hexadecimal format (without the leading ‘0x’).
        /// Required: YES
        /// </summary>
        /// <value>The signing certificate serial number.</value>
        public string signingCertificateSerialNumber { get; set; }
    }
}
