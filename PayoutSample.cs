﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Swish.SampleCode
{
    class PayoutSample
    {
        public static int Main(string[] args)
        {
            PrintStepOne();
            SettingsLoader settingsLoader = new SettingsLoader();
            CertificateService certService = settingsLoader.CreateCertificateService();
            PayoutClient payoutClient = settingsLoader.CreatePayoutClient(certService);
            CreatePayoutData createPayoutData = new CreatePayoutData();
            createPayoutData.payload = settingsLoader.CreatePayload(certService);
            createPayoutData.callbackUrl = settingsLoader.GetCallbackUrl();

            if (certService == null || payoutClient == null)
            {
                Console.WriteLine("\tStep 1 Failed: Could not Load App Settings from app.config. Exiting with -1...");
                return -1;
            }

            PrintStepTwo();
            string payoutLocation = ManageCreatePayout(payoutClient, certService, createPayoutData);
            if (payoutLocation == "")
            {
                Console.WriteLine("\tStep 2 Failed. Exiting with -1...");
                return -1;
            }

            PrintStepThree();
            bool success = payoutClient.PollPayoutStatus(payoutLocation).GetAwaiter().GetResult();
            if (success == false)
            {
                Console.WriteLine("\tStep 3 Failed. Exiting  with -1...");
                return -1;
            }
            return 0;
        }

        /// <summary>
        /// Manages the CreatePayout Call
        /// </summary>
        /// <returns>The payoutInstructionUUID/payoutLocation or empty string if unsuccessful request</returns>
        /// <param name="client">Performs the API requests</param>
        /// <param name="certService">Handles the Certificate Actions</param>
        /// <param name="createPayoutData">Data Class expected by the API</param>
        private static string ManageCreatePayout(PayoutClient client, CertificateService certService, CreatePayoutData createPayoutData)
        {
            ///Create the signature of the payload as a JSON-string.
            createPayoutData.signature = certService
                .CreateSignature(JsonConvert.SerializeObject(createPayoutData.payload));

            var res = client.CreatePayout(createPayoutData).GetAwaiter().GetResult();
            if (res.StatusCode == System.Net.HttpStatusCode.Created)
            {
                var URLparts = res.Headers.Location.ToString().Split('/');
                return URLparts[URLparts.Length - 1];
            }
            Console.WriteLine($"\n\nUnsuccessful Request\nResponse:{res}\nContent: {res.Content.ReadAsStringAsync().GetAwaiter().GetResult()}");
            return "";
        }

        private static void PrintStepOne()
        {
            Console.WriteLine("\n\n\nRunning Payout Sample\n");
            Console.WriteLine("==============================================================================================");
            Console.WriteLine("\tStep 1: Loading Application Settings from file: 'appsettings.json'.");
            Console.WriteLine("==============================================================================================");
        }

        private static void PrintStepTwo()
        {
            Console.WriteLine("==============================================================================================");
            Console.WriteLine("\tStep 2: Creating a Payout");
            Console.WriteLine("==============================================================================================");
        }

        private static void PrintStepThree()
        {
            Console.WriteLine("==============================================================================================");
            Console.WriteLine("\tStep 3: Getting Payout Status using Payout Client");
            Console.WriteLine("==============================================================================================");
        }
    }
}
