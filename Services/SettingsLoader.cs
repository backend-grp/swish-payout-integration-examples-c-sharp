﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;


namespace Swish.SampleCode
{
    /// <summary>
    /// Loads settings from Properties/appsettings.json
    /// </summary>
    public class SettingsLoader
    {

        public static IConfiguration Configuration { get; set; }

        public SettingsLoader()
        {
            var confBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory() + "/Properties/")
                .AddJsonFile("appsettings.json");
            Configuration = confBuilder.Build();
        }

        /// <summary>
        /// Creates the payout client loaded from the connection settings 
        /// </summary>
        /// <see>appsettings.json</see>
        /// <returns>A payout client</returns>
        public PayoutClient CreatePayoutClient(CertificateService certService)
        {
            try
            {
                PayoutClient payoutClient = new PayoutClient(Configuration["PayoutLocationURL"], certService);
                Console.WriteLine("Connection Settings loaded.");
                return payoutClient;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Caught Exception: {e}");
                return null;
            }
        }

        /// <summary>
        /// Creates a certificate service loaded from the certificate settings
        /// </summary>
        /// <see>appsettings.json</see>
        /// <returns>A Certificate Service</returns>
        public CertificateService CreateCertificateService()
        {
            try
            {
                CertificateService certService =  new CertificateService(
                    Configuration["ClientCertPath"],
                    Configuration["SignatureCertPath"],
                    Configuration["CertPassword"]);
                Console.WriteLine("Certificate Settings loaded.");
                return certService;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Caught Exception: {e}");
                return null;
            }
        }

        public string GetCallbackUrl()
        {
            return Configuration["Data:CallbackURL"];
        }

        /// <summary>
        /// Creates the Payload loaded from the Payout Data Settings
        /// </summary>
        /// <see>appsettings.json</see>
        /// <returns>The</returns>
        /// <param name="certService">Performs fetching of PayerAlias and Signing Cert SerialNumber</param>
        public CreatePayoutPayload CreatePayload(CertificateService certService)
        {
            CreatePayoutPayload payload = new CreatePayoutPayload();
            try
            {
                string date = DateTime.Today.ToString("yyyyMMdd");
                payload.payerAlias = certService.GetPayerAliasFromClientCert();
                payload.signingCertificateSerialNumber = certService.GetSerialNumberFromSignCert();
                payload.payoutInstructionUUID = Guid.NewGuid().ToString().Replace("-", "").ToUpper();
                payload.payerPaymentReference = Configuration["Data:PayerPaymentReference"] + date;
                payload.payeeAlias = Configuration["Data:PayeeAlias"];
                payload.payeeSSN = Configuration["Data:PayeeSSN"];
                payload.amount = Configuration["Data:Amount"];
                payload.currency = Configuration["Data:Currency"];
                payload.payoutType = Configuration["Data:PayoutType"];
                payload.message = Configuration["Data:MessageToPayee"];
                Console.WriteLine("Data for Create Payout Request loaded.");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Caught Exception: {e}");
                return null;
            }

            return payload;
        }
    }
}
