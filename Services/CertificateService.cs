﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Swish.SampleCode
{
    /// <summary>
    /// Services for Creating Signature and retrieving information from the Certificates.
    /// </summary>
    public class CertificateService
    {
        private readonly string clientCertPath;
        private readonly string signatureCertPath;
        private readonly string certPassword;

        public CertificateService(string clientCertPath, string signatureCertPath, string certPassword)
        {
            this.clientCertPath = clientCertPath;
            this.signatureCertPath = signatureCertPath;
            this.certPassword = certPassword;
        }

        /// <summary>
        /// Gets the payer alias from client cert.
        /// The Alias is fetched from the Common Name (CN) in Subject name.
        /// </summary>
        /// <returns>The payer alias from client cert.</returns>
        public string GetPayerAliasFromClientCert()
        {
            X509Certificate2 cert = new X509Certificate2(clientCertPath, certPassword, X509KeyStorageFlags.DefaultKeySet);
            return cert.GetNameInfo(X509NameType.SimpleName, false);
        }

        /// <summary>
        /// Gets the Serial Number from Signature Certificate
        /// </summary>
        /// <returns>The serial number from signature cert.</returns>
        public string GetSerialNumberFromSignCert()
        {
            X509Certificate2 cert = new X509Certificate2(signatureCertPath, certPassword, X509KeyStorageFlags.DefaultKeySet);
            return cert.SerialNumber;
        }

        /// <summary>
        /// Creates a SHA512 With RSA Signature of the incoming string. 
        /// </summary>
        /// <returns>The signature string in Base-64</returns>
        /// <param name="stringToSign"></param>
        public string CreateSignature(string stringToSign)
        {
            byte[] payloadHash;
            using (SHA512 sha512 = new SHA512Managed())
            {
                //HASHES the payload string with algoritm SHA512 
                payloadHash = sha512.ComputeHash(Encoding.UTF8.GetBytes(stringToSign));
            }

            X509Certificate2 cert = new X509Certificate2(signatureCertPath, certPassword, X509KeyStorageFlags.DefaultKeySet);
            var rsa = cert.GetRSAPrivateKey();

            //HASHES the payloadHash an additional time before signing it with SHA512 RSA algorithm.
            byte[] signature = rsa.SignData(payloadHash, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
            return Convert.ToBase64String(signature);
        }

        public X509Certificate2 GetClientCert()
        {
            return new X509Certificate2(clientCertPath, certPassword, X509KeyStorageFlags.DefaultKeySet);
        }
    }
}
