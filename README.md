# Payout Sample

This is a small project that implements a REST client towards
the Swish API for making payouts.

## Installation
Developed on Windows platform with 
.NET Core 2.2

Note: The Project is not compatible with linux platforms since its using HttpClient libs from .NET Framework 

- Clone the repository: `git clone https://gitlab.com/backend-grp/swish-payout-integration-examples-c-sharp.git` 
- Build project: `dotnet build`

## Usage
The settings of the app together with the example payout data is set in: [appsettings.json](https://gitlab.shared.getswish.etds.tieto.com/DevOps/payout-sample-csharp/blob/master/PayoutSample/Properties/appsettings.json)

Run project using: `dotnet run bin/Debug/netcoreapp2.2/PayoutSample.dll`
.
## Documentation

Fix this link: [Payout API Specification] (https://developer.getswish.se/merchants/)

## Example run:
```
...\PayoutSample\PayoutSample\bin\Release>PayoutSample


Running Payout Sample
==============================================================================================
    Step 1: Loading Application Settings from file: 'appsettings.json'.
==============================================================================================
    Certificate Settings loaded.
    Connection Settings loaded.
    Data for Create Payout Request loaded.
==============================================================================================
    Step 2: Creating a Payout
==============================================================================================
Sending POST request to: https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/
Request Content:
{
  "payload": {
    "payerAlias": "1232389369",
    "payerPaymentReference": "FirstPrize20190916",
    "payoutInstructionUUID": "7C65213A67544CB7AA48264083551939",
    "payeeAlias": "46768648198",
    "payeeSSN": "196210123235",
    "amount": "55.00",
    "currency": "SEK",
    "payoutType": "PAYOUT",
    "message": "Fishing contest, 1st Prize",
    "signingCertificateSerialNumber": "1234567893",
    "callbackUrl": "https://somewhere.com/callback/"
  },
  "signature": "ogh/GjUrjXL7J3jQTwRtaXJtikWxw+zvmzhRm6jwv95K8TKm3uRYqHjnNuIHPQWiIKaUbf+5RQtwbO7LilAmkZolBsZh6aD8WTo9k6rfEL1q7ZU1+fPVAiEv1uxyNDRSHQ6pWXmjiF1EA6Gmxhxq5ArD/SwDquM6hPMuYAre3tIRnKOOXoO5WYhdbGUwQ/ppufQL6+ENOEdGPCRVNYcI84LuWcVr8/dY35Tjj0irRL9f0tkDFnUU4gXnmMZatiphZEt1EY8WmjzVXMPZDikA0OL1DoexwQTfFnSlreJScjF/n03ft0EJ5noSWe4bRRxVTCTkKrTORkSlDV4uui5nMg=="
}
==============================================================================================
    Step 3: Getting Payout Status using Payout Client
==============================================================================================
Polling Payout Information until status == PAID. 20 tries. 
GET: https://snapshot.getswish.pub.tds.tieto.com/cpc-swish/api/v1/payouts/7C65213A67544CB7AA48264083551939
Try number: 1/20, status = CREATED. Retrying...
Try number: 2/20, status = CREATED. Retrying...
Try number: 3/20, status = DEBITED. Retrying...
Try number: 4/20, status = DEBITED. Retrying...
Try number: 5/20, status = PAID. - Successful payout
Payout Status (GET result):
{
  "paymentReference": "9050534010264720",
  "payoutInstructionUUID": "7C65213A67544CB7AA48264083551939",
  "payerPaymentReference": "FirstPrize20190916",
  "callbackUrl": "https://somewhere.com/callback/",
  "payerAlias": "1232389369",
  "payeeAlias": "46768648198",
  "payeeSSN": "196210123235",
  "amount": 55.0,
  "currency": "SEK",
  "payoutType": "PAYOUT",
  "message": "Fishing contest, 1st Prize",
  "status": "PAID",
  "dateCreated": "2019-09-16T10:44:10.304Z",
  "datePaid": "2019-09-16T10:44:10.875Z",
  "errorMessage": null,
  "additionalInformation": null,
  "errorCode": null
}
```